package dev.unuaamanto.lesson.android.laughing.lessonapp

class Logger() {
    private val logTag = "LessonApp"

    fun log(message: String){
        android.util.Log.i(logTag, message)
    }



    companion object{
        private val logger = Logger()

        fun get(): Logger {
            return logger
        }
    }
}