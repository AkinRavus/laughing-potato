package dev.unuaamanto.lesson.android.laughing.lessonapp

import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity

class ElementsFactory(val context: AppCompatActivity, val layout: LinearLayout) {

    fun create(elementType: ElementType, isNewElement: Boolean = true): Boolean {
        if(isNewElement) {
            Logger.get().log("Create element with type $elementType")
        }

        when(elementType) {
            ElementType.TextInput -> createTextInput()
            ElementType.Button -> createButton()
        }
        return true
    }

    fun create(elementTypeList: List<ElementType>, isNewElement: Boolean = true): Boolean {
        for (elementType in elementTypeList)
            create(elementType, isNewElement)
        return true
    }

    private fun createTextInput() {
        layout.addView(EditText(context))
    }
    private fun createButton() {
        context.layoutInflater.inflate(R.layout.button, layout)
    }
}
 enum class ElementType {
     TextInput,
     Button
 }