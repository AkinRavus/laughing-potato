package dev.unuaamanto.lesson.android.laughing.lessonapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {
    private lateinit var mainLayout: LinearLayout
    private lateinit var elementsFactory: ElementsFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainLayout = findViewById(R.id.layout_main)
        elementsFactory = ElementsFactory(this, mainLayout)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        R.id.action_createButton -> elementsFactory.create(ElementType.Button)

        R.id.action_createInput -> elementsFactory.create(ElementType.TextInput)

        R.id.action_clear -> deleteAllElements()

        else -> super.onOptionsItemSelected(item)
    }

    private fun deleteAllElements(): Boolean {
        mainLayout.removeAllViews()
        return true
    }
}